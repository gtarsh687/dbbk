import React from 'react';
import { Text, View } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import BottomNavigation from './Src/Navigation/BottomNavigation';

const HomeStack = createStackNavigator();



function App() {
  return (
    <>
      <NavigationContainer>
        <HomeStack.Navigator>
          <HomeStack.Screen name="RoleStack" component={BottomNavigation} options={{ headerShown: false }} />
        </HomeStack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default App;

