export const MainColor = "#822B22";
export const SecondColor = "#888";
export const ThirdColor = "#FFF8F6";


export const movies = [
    {
        Name: "Red Mango",
        weight: "500gm",
        quantity: "1",
        price: 20,
        pic: "https://res.cloudinary.com/grohealth/image/upload/$wpsize_!_cld_full!,w_2100,h_1427,c_scale/v1588088840/iStock-467652436.jpg"

    },
    {
        Name: "Cherry",
        weight: "600gm",
        quantity: "2",
        price: 40,
        pic: "https://i0.wp.com/images-prod.healthline.com/hlcmsresource/images/AN_images/blue-fruits-1296x728-feature.jpg?w=1155&h=1528"

    },
    {
        Name: "Apple",
        weight: "200gm",
        quantity: "3",
        price: 80,
        pic: "https://www.mountelizabeth.com.sg/images/default-source/default-album/super-fruits-acai-berry.jpg?sfvrsn=ab92d011_0"

    },
    {
        Name: "Watermelon",
        weight: "2kg",
        quantity: "3",
        price: 150,
        pic: "https://media1.popsugar-assets.com/files/thumbor/rBMlH6V8K60z3QklV_TF1ZfJc-s/fit-in/2048xorig/filters:format_auto-!!-:strip_icc-!!-/2018/07/17/968/n/1922729/479df6855b4e6a263402a0.48863077_/i/Worst-Fruits-Weight-Loss.jpg"
    }
];

