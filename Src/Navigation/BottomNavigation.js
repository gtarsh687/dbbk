import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import DashBoard from '../Screens/DashBoard';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Orders from '../Screens/Orders';
import EditOrders from '../Screens/EditOrders';
import { MainColor, SecondColor } from '../../Common';


const Tabs = createBottomTabNavigator();

export default function BottomNavigation() {
    return (
        <Tabs.Navigator
            // barStyle={{ backgroundColor: "red" }} activeColor="white" inactiveColor="#e2e2e2"
        >
            <Tabs.Screen name="Home" component={DashBoard}
                options={{ title: 'Home', tabBarIcon: ({ focused, color }) => <MaterialIcons name="home" size={27} color={SecondColor} /> }}
            />
            <Tabs.Screen name="Orders" component={Orders}
                options={{ title: 'Orders', tabBarIcon: ({ focused, color }) => <Foundation name="book-bookmark" size={27} color={SecondColor} /> }}
            />
            <Tabs.Screen name="Products" component={EditOrders}
                options={{ title: 'Products', tabBarIcon: ({ focused, color }) => <MaterialCommunityIcons name="dropbox" size={27} color={SecondColor} /> }}
            />
            <Tabs.Screen name="Manage" component={DashBoard}
                options={{ title: 'Manage', tabBarIcon: ({ focused, color }) => <MaterialCommunityIcons name="shape-plus" size={27} color={SecondColor} /> }}
            />
            <Tabs.Screen name="Account" component={Orders}
                options={{ title: 'Account', style: { textColor: "red" }, tabBarIcon: ({ focused, color }) => <Ionicons name="md-person-outline" size={27} color={SecondColor} /> }}
            />
        </Tabs.Navigator>
    )
}

const styles = StyleSheet.create({})
