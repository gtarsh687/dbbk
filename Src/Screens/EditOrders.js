import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Dimensions, ScrollView, Image, TouchableOpacity } from 'react-native'
import { MainColor, SecondColor, movies } from '../../Common';

const { width: WIDTH, height: HEIGHT } = Dimensions.get('window');

export default function EditOrders() {

    useEffect(() => {
        fetch()
    }, []);

    const [Data, updateData] = useState([]);

    async function fetch() {
        let data = await movies
        updateData(data);
    }

    return (
        <View style={styles.MainContainer}>
            <View style={styles.Padder}></View>
            <ScrollView showsVerticalScrollIndicator={false}>

                <View style={styles.TopContainer}>
                    <View style={styles.TopContainer1}><Text style={{ color: '#555', fontSize: 16 }}>{`${Data.length} ITEMS`}</Text></View>
                    <View style={{ width: "80%", height: '100%' }}></View>
                </View>


                <View style={styles.Container}>
                    {
                        Data.map((v, k) => {
                            return (
                                <View style={styles.Product}
                                    key={k}
                                >
                                    <View style={styles.ProductImage}>
                                        <Image
                                            source={{ uri: v.pic }}
                                            style={styles.MainImage}
                                        />
                                    </View>

                                    <View style={styles.ProductContent}>
                                        <View style={styles.ProductUpperContent}>
                                            <View style={styles.ProductName}>
                                                <Text style={styles.ProductText}>
                                                    {/* Red Mango */}
                                                    {v.Name}
                                                </Text>
                                            </View>
                                            <View style={styles.ProductPrice}>
                                                <Text style={styles.ProductText}>$
                                                {/* 200 */}
                                                    {v.price}
                                                </Text>
                                            </View>
                                        </View>

                                        <View style={styles.ProductLowerContent}>
                                            <View style={styles.Productquantity}>
                                                <Text style={styles.ProductText2}>
                                                    {/* 500 gm*/}
                                                    {v.weight}
                                                </Text>
                                            </View>
                                            <View style={[styles.Productunit, { flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }]}>
                                                <View style={{ width: '15%', height: '35%', borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: "#999" }}><Text style={styles.ProductText2}>
                                                    {/* 2 */}
                                                    {v.quantity}
                                                </Text></View>
                                                <Text style={{ color: '#999' }}> x </Text>
                                                <Text style={{ color: '#999' }}> {v.price} </Text>
                                                <View style={{ width: '40%', height: '40%', borderBottom: 4, justifyContent: 'center', alignItems: 'flex-end' }}><Text style={{ color: 'blue' }}>Edit Qty</Text></View>
                                            </View>
                                        </View>

                                    </View>
                                    <View style={styles.Padder}></View>
                                </View>
                            );
                        })
                    }

                    <View style={styles.Bill}>

                        <View style={styles.ItemTotal}>

                            <View style={styles.ItemTotal1}><Text style={{ color: '#555' }}>item Total</Text></View>
                            <View style={styles.ItemTotal2}>
                                <Text style={{ color: '#555' }}>$1.526</Text>
                            </View>


                        </View>

                        <View style={styles.DeliveryFee}>
                            <View style={styles.DeliveryFee1}><Text style={{ color: '#555' }}>Delivery Fee</Text></View>
                            <View style={styles.DeliveryFee2}>
                                <View style={{ width: '70%', height: '50%', borderBottom: 4, justifyContent: 'center', alignItems: 'flex-end' }}><Text style={{ color: '#555' }}>$0</Text></View>
                                <View style={{ width: '30%', height: '50%', borderBottom: 4, justifyContent: 'center', alignItems: 'flex-end' }}><Text style={{ color: 'blue' }}>Edit Qty</Text></View>
                            </View>
                        </View>

                        <View style={{ width: '100%', height: '10%', flexDirection: 'row', borderColor: '#999' }}></View>

                        <View style={styles.GrandTotal}>
                            <View style={styles.GrandTotal1}><Text style={{ color: '#222', fontWeight: 'bold', fontSize: 18 }}>Grand Total</Text></View>
                            <View style={styles.GrandTotal2}>
                                <Text style={{ color: '#555', fontWeight: 'bold', fontSize: 18 }}>$1.526</Text>
                            </View>
                        </View>

                    </View>




                </View>
            </ScrollView>

            <TouchableOpacity style={styles.Button}>
                <Text style={{ color: '#fff', fontSize: 16 }}>Request Confirmation</Text>
            </TouchableOpacity>
            <View style={styles.Padder}></View>

        </View>
    )
}

const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
    },
    TopContainer: {
        marginLeft: 20,
        width: WIDTH - 20,
        height: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    TopContainer1: {
        width: "20%",
        height: '100%',
        borderBottomWidth: 0.5,
        justifyContent: 'center',
    },
    Container: {
        marginLeft: 10,
        width: WIDTH - 20,
        height: HEIGHT,
    },
    Product: {
        marginLeft: "2.5%",
        width: '95%',
        height: '15%',
        flexDirection: 'row',
    },
    ProductImage: {
        width: '30%',
        height: '99%',
        justifyContent: 'center',
    },
    ProductContent: {
        width: '70%',
        height: '99%',
    },
    ProductUpperContent:
    {
        width: '100%',
        height: '40%', flexDirection: 'row'
    },
    ProductName:
    {
        width: '70%',
        height: '100%',
        justifyContent: 'center'
    },
    ProductPrice:
    {
        width: '30%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'flex-end',
        marginRight: 10,


    },
    ProductText:
    {
        marginLeft: '5%',
        color: MainColor
    },
    ProductLowerContent:
    {
        width: '100%',
        height: '60%',
        flexDirection: 'row'
    },
    Productquantity:
    {
        width: '40%',
        height: '100%',
        justifyContent: 'center',
    },
    Productunit:
    {
        width: '60%',
        height: '100%',
    },
    ProductText2:
    {
        marginLeft: '5%',
        color: MainColor
    },
    MainImage: {
        width: "80%",
        height: '80%',
        resizeMode: 'cover'
    },
    Bill: {
        marginLeft: "2.5%",
        width: '95%',
        height: '15%',
        borderTopWidth: 0.8,
        borderColor: '#999'
    },
    ItemTotal: {
        width: '100%',
        height: '30%',
        flexDirection: 'row',
        borderColor: '#999'
    },
    ItemTotal1: {
        width: '50%',
        height: '100%',
        justifyContent: 'center',
    },
    ItemTotal2: {
        width: '50%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    DeliveryFee: {
        width: '100%',
        height: '30%',
        flexDirection: 'row',
        borderColor: '#999'
    },
    DeliveryFee1: {
        width: '50%',
        height: '100%',
        justifyContent: 'center',
    },
    DeliveryFee2: {
        width: '50%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'flex-end',
        flexDirection: 'row'
    },
    GrandTotal: {
        width: '100%',
        height: '30%',
        flexDirection: 'row',
        borderColor: '#999'
    },
    GrandTotal1: {
        width: '50%',
        height: '100%',
        justifyContent: 'center',
    },
    GrandTotal2: {
        width: '50%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    Button: {
        marginLeft: 10,
        width: WIDTH - 20,
        height: 50,
        backgroundColor: MainColor,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
    },
    Padder: {
        marginLeft: "2.5%",
        width: '95%',
        height: '3%',
    }
})
