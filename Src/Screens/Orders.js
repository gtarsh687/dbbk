import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Dimensions, ScrollView, Image, TouchableOpacity } from 'react-native'
import { MainColor, SecondColor, movies, ThirdColor } from '../../Common';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Octicons from 'react-native-vector-icons/Octicons';

const { width: WIDTH, height: HEIGHT } = Dimensions.get('window');

export default function Orders() {

    useEffect(() => {
        fetch()
    }, []);

    const [Data, updateData] = useState([]);

    async function fetch() {
        let data = await movies
        updateData(data);
    }

    return (
        <View style={styles.MainContainer}>

            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.Padder}></View>


                <View style={styles.DateView}>
                    <View style={styles.DateViewMonth}>
                        <Text style={{ color: '#555', fontSize: 16 }}>{`${"Apr"}`}</Text>
                    </View>

                    <View style={styles.DateViewDate}>
                        <Text style={{ color: '#555', fontSize: 16 }}>{`${14}${','}`}</Text>
                    </View>
                    <View style={styles.DateViewTime}>
                        <Text style={{ color: '#555', fontSize: 14 }}>{`${'12'}${":"}${'12'}${'P'}${"M"}`}</Text>
                    </View>

                    <View style={{ width: '35%', height: '100%', }}>
                    </View>
                    <View style={styles.DateViewInfo}>
                        <View style={styles.DateViewInfoIndicator}>
                            <Octicons name="primitive-dot" size={18} color={"orange"} />
                        </View>
                        <View style={styles.DateViewInfoMessage}>
                            <Text>Pending</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.Padder}></View>
                <View style={styles.TopContainer}>
                    <View style={styles.TopContainer1}><Text style={{ color: '#555', fontSize: 16 }}>{`${Data.length} ITEMS`}</Text></View>
                    <View style={{ width: "80%", height: '100%' }}></View>
                </View>

                <View style={styles.Container}>
                    {
                        Data.map((v, k) => {
                            return (
                                <View style={styles.Product}
                                    key={k}
                                >
                                    <View style={styles.ProductImage}>
                                        <Image
                                            source={{ uri: v.pic }}
                                            style={styles.MainImage}
                                        />
                                    </View>

                                    <View style={styles.ProductContent}>
                                        <View style={styles.ProductUpperContent}>
                                            <View style={styles.ProductName}>
                                                <Text style={styles.ProductText}>
                                                    
                                                    {v.Name}
                                                </Text>
                                            </View>
                                            <View style={styles.ProductPrice}>
                                                <Text style={styles.ProductText}>$
                                
                                                    {v.price}
                                                </Text>
                                            </View>
                                        </View>

                                        <View style={styles.ProductLowerContent}>
                                            <View style={styles.Productquantity}>
                                                <Text style={styles.ProductText2}>
                                                   
                                                    {v.weight}
                                                </Text>
                                            </View>


                                            <View style={styles.Productunit}>

                                                <View style={{ width: "60%", height: '100%', justifyContent: 'center', alignItems: "flex-end" }}>
                                                    <View style={{ width: '30%', height: '40%', borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: "#999" }}>
                                                        <Text style={styles.ProductText2}>
                                                            {v.quantity}
                                                        </Text>
                                                        </View>
                                                </View>

                                                <View style={{ width: "20%", height: '100%', justifyContent: 'center', alignItems: "center", }}>
                                                    <Text style={{ fontSize: 16, color: "#999" }}>X</Text></View>
                                                <View style={{ width: "20%", height: '100%', justifyContent: 'center', alignItems: "center", }}>
                                                    <Text>{v.price}</Text>
                                                </View>

                                            </View>


                                        </View>

                                    </View>
                                </View>
                            );
                        })
                    }

                    <View style={styles.Bill}>

                        <View style={styles.ItemTotal}>
                            <View style={styles.ItemTotal1}><Text style={{ color: '#555' }}>item Total</Text></View>
                            <View style={styles.ItemTotal2}>
                                <Text style={{ color: '#555' }}>$1.526</Text>
                            </View>
                        </View>

                        <View style={styles.DeliveryFee}>
                            <View style={styles.DeliveryFee1}><Text style={{ color: '#555' }}>Delivery Fee</Text></View>
                            <View style={styles.DeliveryFee2}>
                                <View style={{ width: '100%', height: '50%', borderBottom: 4, justifyContent: 'center', alignItems: 'flex-end' }}><Text style={{ color: '#555' }}>$0</Text></View>
                            </View>
                        </View>

                        <View style={{ width: '100%', height: '10%', flexDirection: 'row', borderColor: '#999' }}></View>

                        <View style={styles.GrandTotal}>
                            <View style={styles.GrandTotal1}><Text style={{ color: '#222', fontWeight: 'bold', fontSize: 18 }}>Grand Total</Text></View>
                            <View style={styles.GrandTotal2}>
                                <Text style={{ color: '#555', fontWeight: 'bold', fontSize: 18 }}>$1.526</Text>
                            </View>
                        </View>

                    </View>
                </View>
                <View style={{ marginLeft: 10, width: WIDTH - 20, height: 20, }}></View>

                <View style={styles.CustomerDetails}>

                    <View style={styles.CustomerName}>
                        <View style={styles.CustomerName1}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#555' }}>CUSTOMER DETAILS</Text>
                        </View>
                        <View style={{ width: '50%', height: '100%', }}></View>
                    </View>

                    <View style={styles.Padder2}></View>

                    <View style={styles.CustomerName2}><Text style={{ fontSize: 16, fontWeight: 'bold', color: '#555' }}>Fahman Abdul Latheef </Text></View>
                    <View style={styles.CustomerContact}><Text style={{ fontSize: 16, fontWeight: 'bold', color: '#555' }}>9742094729 </Text></View>



                    <View style={styles.Buttons}>

                        <View style={styles.Button1}>
                            <MaterialIcons name="call" size={27} color={"white"} />
                            <Text style={{ fontSize: 18, color: '#fff' }}> Call</Text>
                        </View>
                        <View style={styles.Button2}><MaterialCommunityIcons name="whatsapp" size={27} color={"white"} />
                            <Text style={{ fontSize: 18, color: '#fff' }}> Whatsapp</Text>
                        </View>
                        <View style={{ width: "20%", height: "100%", }}></View>

                    </View>

                    <View style={styles.Padder2}></View>

                    <View style={styles.CustomerAddress}>
                        <View style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#555' }}>Address</Text>
                        </View>
                        <View style={{ width: '75%', height: '100%', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 16, color: '#555' }}>R902,Rohan Jharokha2,Banglore</Text></View>
                    </View>



                    <View style={styles.CustomerCity}>
                        <View style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#555' }}>City</Text>
                        </View>
                        <View style={{ width: '75%', height: '100%', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 16, color: '#555' }}>Banglore</Text></View>
                    </View>




                    <View style={styles.CustomerPinCode}>
                        <View style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#555' }}>PinCode </Text>
                        </View>
                        <View style={{ width: '75%', height: '100%', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 16, color: '#555' }}>560037</Text></View>
                    </View>



                    <View style={styles.CustomerPayment}>
                        <View style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#555' }}>Payment </Text>
                        </View>
                        <View style={{ width: '75%', height: '100%', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 16, color: '#555' }}>Cash On Delivery</Text></View>
                    </View>


                </View>

                <View style={{ marginLeft: 10, width: WIDTH - 20, height: 50, }}></View>
                <View style={{ marginLeft: 10, width: WIDTH - 20, height: 50, }}></View>

            </ScrollView>


        </View >
    )
}

const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
    },
    DateView: {
        marginLeft: 20,
        width: WIDTH - 40,
        height: 30,
        flexDirection: 'row',
        borderBottomWidth: 0.5
    },
    DateViewMonth: {
        width: '10%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    DateViewDate: {
        width: '10%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    DateViewTime: {
        width: '20%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    DateViewInfo: {
        width: '30%',
        height: '100%',
        flexDirection: 'row'
    },
    DateViewInfoIndicator: {
        width: '10%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
        backgroundColor: '#f6deca'
    },
    DateViewInfoMessage: {
        width: '50%',
        height: '100%',
        justifyContent: 'center',
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
        backgroundColor: '#f6deca'
    },
    TopContainer: {
        marginLeft: 20,
        width: WIDTH - 20,
        height: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    TopContainer1: {
        width: "20%",
        height: '100%',
        borderBottomWidth: 0.5,
        justifyContent: 'center',
    },
    Container: {
        marginLeft: 10,
        width: WIDTH - 20,
        height: 550,
    },
    Product: {
        marginLeft: "2.5%",
        width: '95%',
        height: '20%',
        flexDirection: 'row',
    },
    ProductImage: {
        width: '30%',
        height: '99%',
        justifyContent: 'center',
    },
    ProductContent: {
        width: '70%',
        height: '99%',
    },
    ProductUpperContent:
    {
        width: '100%',
        height: '40%',
        flexDirection: 'row'
    },
    ProductName:
    {
        width: '70%',
        height: '100%',
        justifyContent: 'center'
    },
    ProductPrice:
    {
        width: '30%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'flex-end',
        marginRight: 10,


    },
    ProductText:
    {
        marginLeft: '5%',
        color: MainColor
    },
    ProductLowerContent:
    {
        width: '100%',
        height: '60%',
        flexDirection: 'row'
    },
    Productquantity:
    {
        width: '40%',
        height: '100%',
        justifyContent: 'center',
    },
    Productunit:
    {
        width: '60%',
        height: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    ProductText2:{
        marginLeft: '5%',
        color: MainColor
    },
    MainImage: {
        width: "80%",
        height: '80%',
        resizeMode: 'cover'
    },
    Bill: {
        marginLeft: "2.5%",
        width: '95%',
        height: '20%',
        borderTopWidth: 0.8,
        borderColor: '#999'
    },
    ItemTotal: {
        width: '100%',
        height: '30%',
        flexDirection: 'row',
        borderColor: '#999'
    },
    ItemTotal1: {
        width: '50%',
        height: '100%',
        justifyContent: 'center',
    },
    ItemTotal2: {
        width: '50%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    DeliveryFee: {
        width: '100%',
        height: '30%',
        flexDirection: 'row',
        borderColor: '#999'
    },
    DeliveryFee1: {
        width: '50%',
        height: '100%',
        justifyContent: 'center',
    },
    DeliveryFee2: {
        width: '50%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'flex-end',
        flexDirection: 'row'
    },
    GrandTotal: {
        width: '100%',
        height: '30%',
        flexDirection: 'row',
        borderColor: '#999'
    },
    GrandTotal1: {
        width: '50%',
        height: '100%',
        justifyContent: 'center',
    },
    GrandTotal2: {
        width: '50%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    Button: {
        marginLeft: 10,
        width: WIDTH - 20,
        height: 50,
        backgroundColor: MainColor,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
    },
    CustomerDetails: {
        marginLeft: 10,
        width: WIDTH - 20,
        height: 370,
        backgroundColor: "#f6deca",
    },
    CustomerName: {
        marginLeft: 10,
        width: WIDTH - 30,
        height: 40,
        flexDirection: 'row'
    },
    CustomerName1: {
        width: '50%',
        height: '100%',
        borderBottomColor: "#333",
        borderBottomWidth: 0.5,
        justifyContent: 'center',
    },
    CustomerName2: {
        marginLeft: 10,
        width: WIDTH - 30,
        height: 40,
    },
    CustomerContact: {
        marginLeft: 10,
        width: WIDTH - 30,
        height: 40,
    },
    Buttons: {
        marginLeft: 10,
        width: WIDTH - 30,
        height: 40,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    Button1: {
        width: "35%",
        height: "100%",
        backgroundColor: MainColor,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    Button2: {
        width: "35%",
        height: "100%",
        backgroundColor: '#128C7E',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    CustomerAddress: {
        marginLeft: 10,
        width: WIDTH - 30,
        height: 40,
        flexDirection: 'row',
    },
    CustomerCity: {
        marginLeft: 10,
        width: WIDTH - 30,
        height: 40,
        flexDirection: 'row',
    },
    CustomerPinCode: {
        marginLeft: 10,
        width: WIDTH - 30,
        height: 40,
        flexDirection: 'row',
    },
    CustomerPayment: {
        marginLeft: 10,
        width: WIDTH - 30,
        height: 40,
        flexDirection: 'row',
    },
    Padder: {
        marginLeft: "2.5%",
        width: '95%',
        height: '2%',
    },
    Padder2: {
        marginLeft: 10,
        width: WIDTH - 20,
        height: 20
    }

})
