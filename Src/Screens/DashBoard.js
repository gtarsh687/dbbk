import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Dimensions, ScrollView, Image, TouchableOpacity } from 'react-native'
import { MainColor, SecondColor, movies, ThirdColor } from '../../Common';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import AntDesign from 'react-native-vector-icons/AntDesign';

const { width: WIDTH, height: HEIGHT } = Dimensions.get('window');

export default function DashBoard() {

    return (
        <View style={styles.MainContainer}>
            <ScrollView showsVerticalScrollIndicator={false}>

                <View style={styles.MainContainer1}>

                    <View style={styles.DropDownView}>
                        <View style={styles.DropDownViewIcon1}>
                            <MaterialIcons name="storefront" size={22} color={"white"} />
                        </View>
                        <View style={styles.DropDownViewText}>
                            <Text style={{ fontWeight: 'bold', color: "white" }}>White Market</Text>
                        </View>
                        <View style={styles.DropDownViewIcon2}>
                            <MaterialIcons name="keyboard-arrow-down" size={22} color={"white"} />

                        </View>
                        <View style={styles.DropDownViewPadderView}></View>

                        <View style={styles.DropDownViewIndicator}>
                            <View style={styles.DropDownViewIndicatorLeft}>
                                <Text style={{ color: "white" }}>Online</Text>
                            </View>
                            <View style={styles.DropDownViewIndicatorRight}>
                                <Octicons name="primitive-dot" size={26} color={"green"} />
                            </View>
                        </View>
                    </View>

                    <View style={styles.ScreenTitleView}>
                        <View style={styles.ScreenTitleViewLeft}>
                            <Text style={{ fontSize: 26, fontWeight: '600', color: "white" }}>Manage          today's orders</Text>
                        </View>
                        <View style={styles.ScreenTitleViewRight}>

                        </View>

                    </View>

                    <View style={styles.ShareView}>
                        <View style={styles.shareViewLeft}>
                            <AntDesign name="sharealt" size={22} color={"#333"} />
                        </View>
                        <View style={styles.shareViewMiddle}>
                            <View style={styles.shareViewMiddleUp}>
                                <Text style={{ color: "#333", fontSize: 16, fontWeight: 'bold' }}>Share store and earn more</Text>
                            </View>
                            <View style={styles.shareViewMiddleDown}>
                                <Text style={{ color: "#555" }}>beshop.com/whitemarket</Text>
                            </View>
                        </View>
                        <View style={styles.shareViewRight}>
                            <MaterialIcons name="keyboard-arrow-right" size={22} color={"#333"} />
                        </View>
                    </View>


                    <View style={styles.SecondTitleView}>
                        <View style={styles.SecondTitleViewLeft}>
                            <Text style={{ color: "white", fontSize: 16 }}>OVERVIEW</Text>
                        </View>
                        <View style={styles.SecondTitleViewMiddle}>

                        </View>
                        <View style={styles.SecondTitleViewRight}>
                            <View style={styles.SecondTitleViewRight1}>
                                <Text style={{ color: "white" }}>Life time</Text>
                            </View>
                            <View style={styles.SecondTitleViewRight}>
                                <MaterialIcons name="keyboard-arrow-down" size={22} color={"white"} />
                            </View>
                        </View>
                    </View>


                    <View style={styles.BoxView1}>
                        <View style={styles.BoxView1Left}>
                            <View style={styles.BoxView1LeftUpper}>
                                <Text style={{ color: "white", fontSize: 16 }}>Orders</Text>
                            </View>
                            <View style={styles.BoxView1LeftDown}>
                                <Text style={{ color: "white", fontSize: 18, fontWeight: 'bold' }}>2</Text>
                            </View>
                        </View>
                        <View style={styles.BoxView1Right}>
                            <View style={styles.BoxView1RightUpper}>
                                <Text style={{ color: "white", fontSize: 16 }}>Total Sales</Text>
                            </View>
                            <View style={styles.BoxView1RightLower}>
                                <Text style={{ color: "white", fontSize: 18, fontWeight: 'bold' }}>$654</Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.BoxView2}>
                        <View style={styles.BoxView1Left}>
                            <View style={styles.BoxView1LeftUpper}>
                                <Text style={{ color: "white", fontSize: 16 }}>Store Views</Text>
                            </View>
                            <View style={styles.BoxView1LeftDown}>
                                <Text style={{ color: "white", fontSize: 18, fontWeight: 'bold' }}>4</Text>
                            </View>
                        </View>
                        <View style={styles.BoxView1Right}>
                            <View style={styles.BoxView1RightUpper}>
                                <Text style={{ color: "white", fontSize: 16 }}>Product Views</Text>
                            </View>
                            <View style={styles.BoxView1RightLower}>
                                <Text style={{ color: "white", fontSize: 18, fontWeight: 'bold' }}>24</Text>
                            </View>
                        </View>
                    </View>

                </View>

                <View style={styles.MainContainer2}>


                    <View style={styles.ActiveOrderView}>
                        <View style={styles.ActiveOrderViewLeft}>
                            <Text style={{ color: "grey", fontSize: 16, fontWeight: 'bold' }}>ACTIVE ORDERS</Text>
                        </View>
                        <View style={styles.ActiveOrderViewMiddle}></View>
                        <View style={styles.ActiveOrderViewRight}>
                            <View style={styles.ActiveOrderViewRightright}>
                                <Text style={{ color: "grey", fontSize: 16, fontWeight: 'bold' }}>View All</Text>
                            </View>
                            <View style={styles.ActiveOrderViewRightleft}>
                                <MaterialIcons name="keyboard-arrow-right" size={22} color={"#333"} />
                            </View>
                        </View>
                    </View>

                    <View style={styles.OrderStatusView}>
                        <View style={styles.OrderStatusViewLeft}>
                            <Text style={{ color: "#222", fontSize: 16, fontWeight: 'bold' }}>Pending</Text>
                        </View>
                        <View style={styles.OrderStatusViewMiddle}>
                            <Text style={{ color: "grey", fontSize: 16, fontWeight: 'bold' }}>Accepted</Text>
                        </View>
                        <View style={styles.OrderStatusViewRight}>
                            <Text style={{ color: "grey", fontSize: 16, fontWeight: 'bold' }}>Shipped</Text>
                        </View>
                    </View>

                    <View style={styles.CardView}>

                        <View style={styles.CardViewUpper}>
                            <View style={styles.CardViewUpperLeft}>
                                <Image
                                    source={{ uri: "https://res.cloudinary.com/grohealth/image/upload/$wpsize_!_cld_full!,w_2100,h_1427,c_scale/v1588088840/iStock-467652436.jpg" }}
                                    style={styles.MainImage}
                                />
                            </View>
                            <View style={styles.CardViewUpperMiddle}>
                                <View style={styles.CardViewUpperMiddleUpper}>
                                    <Text style={{ color: "#444", fontSize: 16 }}>Order #1438165</Text>
                                </View>
                                <View style={styles.CardViewUpperMiddleLower}>
                                    <Text style={{ color: "#444", fontSize: 16 }}>4 Items | Aril 14, 12:10Pm</Text>
                                </View>
                            </View>
                            <View style={styles.CardViewUpperRight}>
                                <Text style={{ color: "blue", fontSize: 16, fontWeight: 'bold' }}>$654</Text>
                            </View>
                        </View>

                        <View style={styles.CardViewMiddle}></View>

                        <View style={styles.CardViewLower}>
                            <View style={styles.DateViewInfo}>
                                <View style={styles.DateViewInfoIndicator}>
                                    <Octicons name="primitive-dot" size={18} color={"orange"} />
                                </View>
                                <View style={styles.DateViewInfoMessage}>
                                    <Text>Pending</Text>
                                </View>
                            </View>
                            <View style={styles.DateViewInfoMiddle}>
                                <View style={{ width: '30%', height: '50%', borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: "#999" }}>
                                    <Text style={styles.ProductText2}>
                                        COD
                                    </Text></View>
                            </View>
                            <View style={styles.DateViewInfoRight}>
                                <View style={styles.DateViewInfoRightright}>
                                    <Text style={{ color: "grey", fontSize: 16, fontWeight: 'bold' }}>Details</Text>
                                </View>
                                <View style={styles.DateViewInfoRightleft}>
                                    <MaterialIcons name="keyboard-arrow-right" size={22} color={"#333"} />
                                </View>
                            </View>
                        </View>
                    </View>


                    <View style={styles.CardView}>

                        <View style={styles.CardViewUpper}>
                            <View style={styles.CardViewUpperLeft}>
                                <Image
                                    source={{ uri: "https://i0.wp.com/images-prod.healthline.com/hlcmsresource/images/AN_images/blue-fruits-1296x728-feature.jpg?w=1155&h=1528" }}
                                    style={styles.MainImage}
                                />
                            </View>
                            <View style={styles.CardViewUpperMiddle}>
                                <View style={styles.CardViewUpperMiddleUpper}>
                                    <Text style={{ color: "#444", fontSize: 16 }}>Order #1438165</Text>
                                </View>
                                <View style={styles.CardViewUpperMiddleLower}>
                                    <Text style={{ color: "#444", fontSize: 16 }}>4 Items | Aril 14, 12:10Pm</Text>
                                </View>
                            </View>
                            <View style={styles.CardViewUpperRight}>
                                <Text style={{ color: "blue", fontSize: 16, fontWeight: 'bold' }}>$654</Text>
                            </View>
                        </View>

                        <View style={styles.CardViewMiddle}></View>

                        <View style={styles.CardViewLower}>
                            <View style={styles.DateViewInfo}>
                                <View style={styles.DateViewInfoIndicator}>
                                    <Octicons name="primitive-dot" size={18} color={"orange"} />
                                </View>
                                <View style={styles.DateViewInfoMessage}>
                                    <Text>Pending</Text>
                                </View>
                            </View>
                            <View style={styles.DateViewInfoMiddle}>
                                <View style={{ width: '30%', height: '50%', borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: "#999" }}>
                                    <Text style={styles.ProductText2}>
                                        COD
                                    </Text></View>
                            </View>
                            <View style={styles.DateViewInfoRight}>
                                <View style={styles.DateViewInfoRightright}>
                                    <Text style={{ color: "grey", fontSize: 16, fontWeight: 'bold' }}>Details</Text>
                                </View>
                                <View style={styles.DateViewInfoRightleft}>
                                    <MaterialIcons name="keyboard-arrow-right" size={22} color={"#333"} />
                                </View>
                            </View>
                        </View>
                    </View>


                </View>

                <View style={styles.Padder}></View>
                <View style={styles.Padder}></View>
                <View style={styles.Padder}></View>

            </ScrollView>
        </View >
    )
}

const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: MainColor
    },
    MainContainer1: {
        marginTop: 5,
        marginLeft: 10,
        width: WIDTH - 20,
        height: 450,
    },
    DropDownView: {
        paddingLeft: 5,
        width: "100%",
        height: '10%',
        flexDirection: 'row'
    },
    DropDownViewIcon1: {
        width: "8%",
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    DropDownViewText: {
        width: "24%",
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    DropDownViewIcon2: {
        width: "5%",
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    DropDownViewPadderView: {
        width: "38%",
        height: '100%',
    },
    DropDownViewIndicator: {
        width: "25%",
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    DropDownViewIndicatorLeft: {
        width: '60%',
        height: '60%',
        backgroundColor: "#34120D",
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    DropDownViewIndicatorRight: {
        width: '20%',
        height: '60%',
        backgroundColor: '#34120D',
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },

    ScreenTitleView: {
        paddingLeft: 10,
        width: "100%",
        height: '25%',
        backgroundColor: MainColor,
        flexDirection: 'row',
    },
    ScreenTitleViewLeft: {
        width: '45%',
        height: '90%',
        justifyContent: 'center',
        borderBottomWidth: 0.5,
        borderColor: 'orange'
    },
    ScreenTitleViewRight: {
        width: '40%',
        height: '90%',
    },
    ShareView: {
        width: "100%",
        height: '20%',
        backgroundColor: MainColor,
        flexDirection: 'row',
        paddingLeft: 10,
    },
    shareViewLeft: {
        width: '20%',
        height: '80%',
        backgroundColor: 'white',
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    shareViewMiddle: {
        width: '62%',
        height: '80%',
        backgroundColor: 'white',
        justifyContent: 'center',
    },
    shareViewMiddleUp: {
        width: '100%',
        height: '40%',
        justifyContent: 'flex-end',
    },
    shareViewMiddleDown: {
        width: '100%',
        height: '40%',
        backgroundColor: 'white',
        justifyContent: 'flex-start',
    },
    shareViewRight: {
        width: '15%',
        height: '80%',
        backgroundColor: 'white',
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    SecondTitleView: {
        width: "100%",
        height: '10%',
        backgroundColor: MainColor,
        flexDirection: 'row',
        paddingLeft: 10,
    },
    SecondTitleViewLeft: {
        width: '22%',
        height: '80%',
        borderBottomWidth: 0.5,
        borderBottomColor: 'orange',
        justifyContent: 'center',
    },
    SecondTitleViewMiddle: {
        width: '45%',
        height: '80%',
    },
    SecondTitleViewRight: {
        width: '30%',
        height: '80%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    BoxView1: {
        width: "100%",
        height: '16%',
        backgroundColor: MainColor,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    BoxView1Left: {
        width: '45%',
        height: '90%',
        backgroundColor: "#9C3328",
        borderRadius: 5,
        paddingLeft: 10
    },
    BoxView1LeftUpper: {
        width: '100%',
        height: '40%',
    },
    BoxView1LeftDown: {
        width: '100%',
        height: '40%',
    },
    BoxView1Right: {
        width: '45%',
        height: '90%',
        backgroundColor: '#9C3328',
        borderRadius: 5,
        paddingLeft: 10
    },
    BoxView1RightUpper: {
        width: '100%',
        height: '40%',
    },
    BoxView1RightDown: {
        width: '100%',
        height: '40%',
    },
    BoxView2: {
        width: "100%",
        height: '16%',
        backgroundColor: MainColor,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    MainContainer2: {
        width: WIDTH,
        height: 600,
        backgroundColor: 'white',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
    },
    ActiveOrderView: {
        marginTop: 40,
        marginLeft: 20,
        width: WIDTH - 40,
        height: '5%',
        flexDirection: 'row',
    },
    ActiveOrderViewLeft: {
        width: '35%',
        height: '95%',
        borderBottomColor: 'grey',
        borderBottomWidth: 1,

    },
    ActiveOrderViewMiddle: {
        width: '38%',
        height: '95%',
    },
    ActiveOrderViewRight: {
        width: '25%',
        height: '95%',
        flexDirection: 'row',
    },
    ActiveOrderViewRightright: {
        width: '80%',
        height: '95%',
        alignItems: 'center',
    },
    ActiveOrderViewRightleft: {
        width: '20%',
        height: '95%',
        alignItems: 'center',
    },
    OrderStatusView: {
        marginTop: 10,
        marginLeft: 20,
        width: WIDTH - 40,
        height: '8%',
        flexDirection: 'row',
        justifyContent: 'space-around',
        borderBottomWidth: 0.9,
        borderBottomColor: 'grey',
    },
    OrderStatusViewLeft: {
        width: '30%',
        height: '95%',
        borderBottomColor: MainColor,
        borderBottomWidth: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    OrderStatusViewMiddle: {
        width: '30%',
        height: '95%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    OrderStatusViewRight: {
        width: '30%',
        height: '95%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    CardView: {
        marginTop: 10,
        marginLeft: 20,
        width: WIDTH - 40,
        height: '25%',
        flexDirection: 'column',
        backgroundColor: ThirdColor
    },
    CardViewUpper: {
        width: '99%',
        height: '55%',
        flexDirection: 'row',

    },
    CardViewUpperLeft: {
        width: '20%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 0.9,
        borderBottomColor: '#333'

    },
    MainImage: {
        width: "80%",
        height: '80%',
        resizeMode: 'cover'
    },

    CardViewUpperMiddle: {
        width: '60%',
        height: '100%',
        borderBottomWidth: 0.9,
        borderBottomColor: '#333'
    },
    CardViewUpperMiddleUpper: {
        height: '50%',
        width: '100%',
        paddingLeft: 10,
        justifyContent: 'center',
    },
    CardViewUpperMiddleLower: {
        height: '50%',
        width: '100%',
        alignItems: 'center'
    },
    CardViewUpperRight: {
        width: '20%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 0.9,
        borderBottomColor: '#333'
    },
    CardViewMiddle: {
        width: '99%',
        height: '15%',
        flexDirection: 'row',
    },
    CardViewLower: {
        width: '99%',
        height: '35%',
        flexDirection: 'row'
    },
    DateViewInfo: {
        width: '33%',
        height: '100%',
        flexDirection: 'row',
    },
    DateViewInfoIndicator: {
        width: '30%',
        height: '50%',
        alignItems: 'center',
        justifyContent: 'center',
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
        backgroundColor: '#f6deca'
    },
    DateViewInfoMessage: {
        width: '50%',
        height: '50%',
        justifyContent: 'center',
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
        backgroundColor: '#f6deca'
    },
    DateViewInfoMiddle: {
        width: '40%',
        height: '100%',
        flexDirection: 'row',
    },
    ProductText2: {
        marginLeft: '5%',
        color: MainColor
    },
    DateViewInfoRight: {
        width: '20%',
        height: '100%',
        flexDirection: 'row',
    },
    DateViewInfoRightright: {
        width: '80%',
        height: '95%',
        alignItems: 'center',
    },
    DateViewInfoRightleft: {
        width: '20%',
        height: '95%',
        alignItems: 'center',
    },
    Padder: {
        marginLeft: "2.5%",
        width: '95%',
        height: '3%',
        backgroundColor: 'green'
    }
})
